import p5 from "p5";
import DefaultImageSamplingStrategy from "./DefaultImageSamplingStrategy";
import ImageSampler from "./ImageSampler";
import ImageStore from "./ImageStore";

class HeightmapSketch {

	private canvas?: p5.Renderer;
	// Every x/y Pixels, a Height-Position is sampled
	private sampleDensity = {x: 10, y: 5};
	// When drawing, every x/y Pixels a new Vertex is added.
	private lengthPerSample = {x: 5, y: 2};
	private amplitude = 150;
	private margin = 50;
	private img!: p5.Image

	private samples!: number[][];
	private totalSamples!: {x: number, y: number}
	private currentY = 0;
	private finished = false;
	private canvasSize!: { x: number; y: number; };

	constructor(private p: p5){
		p.preload = () => this.preload();
		p.setup = () => this.setup();
		p.draw = () => this.draw();
	}

	private preload() {
		this.img = this.p.loadImage(HeightmapSketch.selectImage(window.location.hash));
	}

	private setup() {
		// Style for non-masking:
		this.p.background("#000000");

		this.totalSamples = {x: Math.floor(this.img.width / this.sampleDensity.x), y: Math.floor(this.img.height / this.sampleDensity.y)};
		this.canvasSize = {x: this.totalSamples.x * this.lengthPerSample.x + 2 * this.margin, y: this.totalSamples.y * this.lengthPerSample.y + 2 * this.margin};
		const sampler = new ImageSampler(new DefaultImageSamplingStrategy());
		this.samples = sampler.sampleImage(this.img, this.sampleDensity, this.totalSamples);

		this.canvas = this.p.createCanvas(this.canvasSize.x, this.canvasSize.y);
		this.canvas.parent("app");
		this.p.background("#000000");
	}


	private draw() {

		
		if(this.finished) {
			return;
		}

		const yModifier = -this.amplitude;

		const line = [];
		for(let x = 0; x < this.totalSamples.x; x++) {
	
			const sample = this.samples[this.currentY][x];
			if(sample === undefined) {
				console.error(`Sample ${x}, ${this.currentY} is undefined!`)
				continue;
			}
			line.push( sample * yModifier )
		}
		const baselineY = this.margin + (this.currentY + 1) * this.lengthPerSample.y;
		const lineAsCoords = line.map( (e, i) => {
			return {
				x: this.margin + i * this.lengthPerSample.x,
				y: baselineY + e
			}
		})



		// Masking
		
		this.p.noStroke()
		this.p.fill("#000000")
		this.p.beginShape();
		// Draw a polygon that will mask anything below this rows line.
		const polyLine: {x: number, y: number}[] = [];
		polyLine.push(
			//{x: this.margin, y: baselineY},
			...lineAsCoords,
			//{x: this.canvasSize.x, y: baselineY},
			{x: this.canvasSize.x - this.margin, y: this.canvasSize.y - this.margin},
			{x: this.margin, y: this.canvasSize.y - this.margin}
		)
		this.p.beginShape();
		for(const {x,y} of polyLine) {
			this.p.vertex(x,y);
		}
		this.p.endShape("close");
		
		// Line
		
		this.p.noFill();
		this.p.stroke("orange");
		this.p.beginShape();
		for(const {x,y} of lineAsCoords) {
			this.p.vertex(x,y)
		}
		this.p.endShape();
		

		this.currentY++;
		if(this.currentY >= this.totalSamples.y){
			this.finished = true;
		}

	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new HeightmapSketch(p5);
	}

	private static selectImage(hash: string) {

		const split = hash.split("#");
		if(split.length < 2) {
			return ImageStore.lakeConstance;
		}
		else {
			const value = split[1].trim();
			switch(value) {
				case "australia": return ImageStore.australia;
				case "garda": return ImageStore.gardasee;
				default: return ImageStore.lakeConstance;
			}
		}
	}
}

export default HeightmapSketch;
