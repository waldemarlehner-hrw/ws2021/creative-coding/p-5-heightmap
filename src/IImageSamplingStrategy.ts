interface IImageSamplingStrategy {
    /**
     * @param r 0-255
     * @param g 0-255
     * @param b 0-255
     * @returns 0-1
     */
    sample(r: number, g: number, b: number): number
}