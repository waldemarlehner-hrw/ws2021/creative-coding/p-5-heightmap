import './style.css'
import P5 from "p5";
import HeightmapSketch from "./HeightmapSketch"

const sketch = HeightmapSketch.toP5Sketch();

new P5(sketch);
