import constanceurl from "../img/input.png";
import australiaurl from "../img/australia.png";
import gardaurl from "../img/gardasee.png";

class ImageStore {
    public static get lakeConstance() {
        return constanceurl;
    }

    public static get australia() {
        return australiaurl;
    }

    public static get gardasee() {
        return gardaurl;
    }
}

export default ImageStore;