class DefaultImageSamplingStrategy implements IImageSamplingStrategy {
    constructor(){}

    sample(r: number, g: number, b: number): number {
        // Average r,g,b, then map [0,255] => [0,1]

        const avg = (r+g+b) / 3;
        const returnValue = avg / 255;
        if(Number.isNaN(returnValue)) {
            console.warn(r,g,b,"returned nan");
        }
        return returnValue; 
    }
}

export default DefaultImageSamplingStrategy;