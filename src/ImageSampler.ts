import p5 from "p5";

class ImageSampler {
    constructor(private samplerStrategy: IImageSamplingStrategy) {}

    public sampleImage(img: p5.Image, sampleDensity: {x: number, y: number}, totalCount: {x: number, y:number}) {
        const map = [];
        const [xCount, yCount, sampleDensityX, sampleDensityY] = [totalCount.x, totalCount.y, sampleDensity.x, sampleDensity.y];

        for(let y = 0; y < yCount; y++) {
            
            const row = [];

            for(let x = 0; x < xCount; x++) {
                const sampleX = sampleDensityX * x;
                const sampleY = sampleDensityY * y;

                const [r,g,b] = img.get(sampleX, sampleY);
                const value = this.samplerStrategy.sample(r,g,b);
                row.push(value);
            }
            map.push(row);
        }

        return map;
    }

}

export default ImageSampler;